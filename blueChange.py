def changeBlue(picture):
  xMax = getWidth(picture)
  yMax = getHeight(picture)
  for col in range(0, xMax):
    for row in range(0, yMax):
      pixel = getPixelAt(picture, col, row)
      if(getBlue(pixel) < 150):
        setColor(pixel, white)
        
  return picture
               
file =pickAFile()
myPic = makePicture(file)
changedPic = changeBlue(myPic)
show(changedPic)


